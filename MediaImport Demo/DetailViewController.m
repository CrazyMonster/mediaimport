//
//  DetailViewController.m
//  MediaImport Demo
//
//  Created by Elia on 04/09/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item
@synthesize isAvailable;
@synthesize isCurrent;

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.title = [self.detailItem description];
        self.isAvailable.text = (self.detailItem.isAvailable)? @"YES" : @"NO";
        self.isCurrent.text = ([[MediaImport currentService] isEqual:self.detailItem])? @"YES" : @"NO";
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewDidUnload
{
    [self setIsAvailable:nil];
    [self setIsCurrent:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"-", @"-");
    }
    return self;
}
							
- (IBAction)makeCurrent:(id)sender {
    [[MediaImport sharedInstance] setUserChosenService:self.detailItem];
    [self configureView];
}
@end
