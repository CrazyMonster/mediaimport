//
//  MasterViewController.h
//  MediaImport Demo
//
//  Created by Elia on 04/09/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
