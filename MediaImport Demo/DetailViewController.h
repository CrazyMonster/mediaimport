//
//  DetailViewController.h
//  MediaImport Demo
//
//  Created by Elia on 04/09/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MediaImport/MediaImport.h>

@interface DetailViewController : UIViewController
@property (strong, nonatomic) MIService *detailItem;

- (IBAction)makeCurrent:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *isAvailable;
@property (weak, nonatomic) IBOutlet UILabel *isCurrent;

@end
