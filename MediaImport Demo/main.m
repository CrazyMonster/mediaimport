//
//  main.m
//  MediaImport Demo
//
//  Created by Elia on 04/09/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
