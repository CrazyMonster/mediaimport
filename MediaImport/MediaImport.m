//
//  MediaImport.m
//  MediaImport
//
//  Created by CrazyMonster on 18/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MediaImport.h>

#import <MediaImport/MIAirBlueSharingService.h>
#import <MediaImport/MIGremlinService.h>
#import <MediaImport/MIMewSeekService.h>
#import <MediaImport/MIOpenInService.h>

typedef const NSString MIServiceKey;

MIServiceKey *MIAirBlueServiceKey = @"MIAirBlueService";
MIServiceKey *MIGremlinServiceKey = @"MIGremlinService";
MIServiceKey *MIMewSeekServiceKey = @"MIMewSeekService";
MIServiceKey *MIOpenInServiceKey = @"MIOpenInService";

#define kMIChosenServiceKey @"MIChosenService"

@interface MediaImport ()
{
    NSDictionary *_servicesDictionary;
    NSArray *_supportedServices;
}

@property (readonly) NSDictionary *servicesDictionary;
@property (retain) MIServiceKey *defaultServiceKey;

- (MIServiceKey *)__keyForService:(MIService *)service;
- (void)__setUserChosenServiceWithKey:(MIServiceKey *)serviceKey;
- (MIService *)__firstAvailableService;

@end

@implementation MediaImport

+ (MediaImport *)sharedInstance
{
    static MediaImport *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[MediaImport alloc] init];
        NSParameterAssert(_instance != nil);
    });
    
    return _instance;
}

- (id)init
{
    if (self = [super init]) {
        self.applicationIdentifier = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
        self.defaultServiceKey = MIGremlinServiceKey;
    }
    
    return self;
}

- (NSDictionary *)servicesDictionary
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _servicesDictionary = @{
            //MIAirBlueServiceKey: [MIAirBlueSharingService sharedService],
            MIGremlinServiceKey: [MIGremlinService sharedService],
            //MIMewSeekServiceKey: [MIMewSeekService sharedService],
            //MIOpenInServiceKey: [MIOpenInService sharedService]
        };
        
        NSParameterAssert(_servicesDictionary != nil);
    });
    
    return _servicesDictionary;
}
- (MIService *)defaultService
{
    return self.servicesDictionary[self.defaultServiceKey];
}
- (void)setDefaultService:(MIService *)defaultService
{
    self.defaultServiceKey = [self __keyForService:defaultService];
}
- (NSArray *)supportedServices
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{        
        _supportedServices = self.servicesDictionary.allValues;
        NSParameterAssert(_supportedServices != nil);
    });
    return _supportedServices;
}
- (NSArray *)availableServices
{
    NSMutableArray *mutableAvailableServices = [NSMutableArray array];
    [self.supportedServices enumerateObjectsUsingBlock:^(MIService *service, NSUInteger idx, BOOL *stop) {
        NSAssert([service isKindOfClass:[MIService class]] == YES, @"Invalid object returned by self.supportedServices: %@", service);
        
        if (service.isAvailable == YES) {
            [mutableAvailableServices addObject:service];
        }
    }];
    
    NSArray *availableServices = [mutableAvailableServices copy];
    NSParameterAssert(availableServices != nil);
    
    return availableServices;
}

- (MIServiceKey *)__keyForService:(MIService *)service
{
    NSArray *allKeys = [self.servicesDictionary allKeysForObject:service];
    NSAssert(allKeys.count == 1, @"Service %@ appears more than 1 time (%u times) in self.supportedServices", service, allKeys.count);
    
    NSString *key = allKeys[0];
    NSAssert([key isKindOfClass:[NSString class]] == YES, @"Invalid key %@ for service %@", key, service);
    
    return key;
}
- (void)__setUserChosenServiceWithKey:(MIServiceKey *)serviceKey
{
    NSLog(@"Selecting importer service %@", serviceKey);
    
    [[NSUserDefaults standardUserDefaults] setObject:serviceKey forKey:kMIChosenServiceKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)setUserChosenService:(MIService *)service
{
    [self __setUserChosenServiceWithKey:[self __keyForService:service]];
}

- (MIService *)__firstAvailableService
{
    NSArray *availableServices = self.availableServices;
    
    if (availableServices.count != 0) {
        return availableServices[0];
    } else {
        return nil;
    }
}

- (MIService *)currentService
{
    MIServiceKey *userChoice = [[NSUserDefaults standardUserDefaults] objectForKey:kMIChosenServiceKey];
    MIServiceKey *serviceKey = (userChoice != nil) ? userChoice : self.defaultServiceKey;
    
    MIService *currentService = self.servicesDictionary[serviceKey];
    NSAssert(currentService != nil, @"Trying to retrieve service for invalid key %@ (is user choice: %@)", serviceKey, @((userChoice != nil)));
    NSAssert([currentService isKindOfClass:[MIService class]] == YES, @"Invalid object returned by self.supportedServices: %@", currentService);

    if (currentService.isAvailable == NO) {
        currentService = [self __firstAvailableService];
    }
    
    return currentService;
}
+ (MIService *)currentService
{
    return [[self sharedInstance] currentService];
}

@end
