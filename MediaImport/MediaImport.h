//
//  MediaImport.h
//  MediaImport
//
//  Created by CrazyMonster on 18/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MediaImport/MIService.h>
#import <MediaImport/MISupportedFeatures.h>

/** Main class of the **MediaImport** library. It provides facilities for service inquiring and file importing.
 
 This should be the only class you access directly, using then its methods to access service classes, instead of referencing them directly. Doing so allows your application to be completely service agnostic: it only needs to know if a service is available and request it to import files. This way your application will be compatible with all services supported by **MediaImport**, even future ones with the same architecture. It’s _strongly_ recommended to use this approach to allow users to pick up the import service they prefer.
 
 _Future versions will also allow to add custom services using subclasses of the MIService base abstract class._
 
 @warning This is a singleton class, therefore you should obtain class instances from sharedInstance. You **MUST** not use the alloc-init initialization, the behavior of doing so is undefined.
 */
@interface MediaImport : NSObject

///---------------------------------------------------------------------------------------
/// @name Class methods
///---------------------------------------------------------------------------------------

/** Method for retrieving the `MediaImport` singleton.
 
 @return The class's singleton instance that can be used to access **MediaImport** functionalities.
 */
+ (MediaImport *)sharedInstance;

/** Convenience alias for the `- currentService` instance method.
 
 @warning The object returned by this method shouldn't be cached: a service can become unavailable while your application is running and if you cache this object you wouldn't notice the change. Instead, call this method every time you need it.
 
 @return The current service selected by the user or the first available service returned by availableServices.
 */
+ (MIService *)currentService;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** An array of all services currently supported by **MediaImport**.
 
 _Future versions will also allow to add custom services using subclasses of the MIService base abstract class. The API for accomplish this is still undefined._
 */
@property (readonly)    NSArray *supportedServices;

/** An array of all services available on the current device.
 
 An available service is defined as a service with the [isAvailable]([MIService isAvailable]) property set to at `YES` the moment of the request, searched among those returned by [supportedServices](supportedServices). [isAvailable]([MIService isAvailable]) implementation is left to single services, to allow them to best determine the availability of required components.
 
 @warning The array returned by this property shouldn't be cached: a service can become unavailable while your application is running and if you cache this object you wouldn't notice the change. Instead, load it from this property every time you need it.
 */
@property (readonly)    NSArray *availableServices;

/** Default service to use in case the user haven’t selected a preferred service yet.
 
 `MediaImport` clients should set it shortly after every application launch (i.e in `UIApplication` delegate’s `application:didFinishLaunchingWithOptions:`). Possible values are the objects in supportedServices array. The default value is the MewSeek service.
 */
@property               MIService *defaultService;

/** An application identifier string that is passed to services as a reference of the app that requested the import.
 
 The default value is the `CFBundleIdentifier` in the application’s Info.plist, so typical client applications shouldn’t need to change its value. This property is present to allow tweak developers to provide an accurate identifier.
 @warning By default, when **MediaImport** is used in a MobileSubstrate tweak, applicationIdentifier represent the bundle identifier of the hooked application because tweaks don't have an Info.plist on their own. Adjust this value accordingly.
 @warning Some services may not support this property (i.e. MewSeek), the application’s `CFBundleIdentifier` is used instead and this cannot be changed. This is a UIKit limitation that occurs when communication with the service happens via URL passing.
 */
@property (retain)      NSString *applicationIdentifier;

///---------------------------------------------------------------------------------------
/// @name Instance methods
///---------------------------------------------------------------------------------------

/** Set the preferred service as selected by the user.
 
 This preference should expressed explicitly by the user, by presenting an option in the application’s Settings or in a similar manner. You should not use this to store, i.e. the last used service, some service you chose or any similary concept. This is to ensure to the users a consistent behavior and give them the ability to choose.
 
 @param service A MIService subclass that represent the service chosen by the user, among those returned by supportedServices. Anyway, to be consistent you should only set this to services in the availableServices array (if a service isn’t available it couldn’t be used anyway, so it’s useless to let the user select it).
 */
- (void)setUserChosenService:(MIService *)service;

/** The instance returned by sharedService of the current service. There is a convenience class method that provides identical functionality: [+ currentService](currentService).
 
 @warning The object returned by this method shouldn't be cached: a service can become unavailable while your application is running and if you cache this object you wouldn't notice the change. Instead, call this method every time you need it.
 
 @return The current service selected by the user or the default service if user haven’t specified her preference. If this isn’t available, the first available service returned by availableServices is used.
 @see +currentService
 */
- (MIService *)currentService;

@end
