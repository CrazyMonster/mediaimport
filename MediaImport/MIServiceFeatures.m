//
//  MIServiceFeatures.h
//  MediaImport
//
//  Created by CrazyMonster on 31/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIServiceFeatures.h>

const MIServiceFeatures MIImportLeavesApplication = @"MIImportLeavesApplication";

const MIServiceFeatures MISupportsAudioFiles = @"MISupportsAudioFiles";
const MIServiceFeatures MISupportsContactFiles = @"MISupportsContactFiles";
const MIServiceFeatures MISupportsImageFiles = @"MISupportsImageFiles";
const MIServiceFeatures MISupportsTextFiles = @"MISupportsTextFiles";
const MIServiceFeatures MISupportsVideoFiles = @"MISupportsVideoFiles";

const MIServiceFeatures MISupportsSongTitleMetadata = @"MISupportsSongTitleMetadata";
const MIServiceFeatures MISupportsSongArtistMetadata = @"MISupportsSongArtistMetadata";
const MIServiceFeatures MISupportsSongAlbumMetadata = @"MISupportsSongAlbumMetadata";
const MIServiceFeatures MISupportsSongAlbumArtworkMetadata = @"MISupportsSongAlbumArtworkMetadata";
