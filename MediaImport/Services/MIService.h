//
//  MIService.h
//  MediaImport
//
//  Created by CrazyMonster on 18/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaImport/MIServiceFeatures.h>

/** The abstract class that provides a scaffold for implementing real services. It's used as the base for all built-in services that come with MediaImport: AirBlue Sharing, Gremlin and, while still <u>experimental</u>, MewSeek. Client apps shouldn't access this directly, this is a facility for service providers to implement custom services.
 
 ## Subclassing notes ##
 This is an abstract class that's is meant to be subclassed to implement a concrete behavior. While it's already possible to create custom services by subclassing MIService, at the moment the MediaImport class doesn't support custom services. _This feature will come in a future update._
 
 ### Methods to Override ###
 For typical services there are just three methods you have to override, look at the method's documentation for more detailed advices on how you should implement them:
 
 * `init`, use it to initialize the value of info properties and any other initialization your service might need.
 * `isAvailable`, provide a reliable method to determine availability of your service.
 * `importFilesInArray:`, do the real communication with your service to trigger the import.
 
 One other method is optional to override, because it's default implementation is concrete even in MIService: `importFileAtPath:` default implementation uses importFilesInArray: to import single files, by passing it an array containing only tha `path` parameter.
 Other methods should **NOT** be overridden. The properties' values should be set in the `init` method.
 
 @warning This is an abstract class, provided only to be subclassed, you should not at any time try to obtaining an instance or access its methods. Doing so would result in a runtime exception leading to an **application crash**.
 */
@interface MIService : NSObject

///---------------------------------------------------------------------------------------
/// @name Class methods
///---------------------------------------------------------------------------------------

/** Method for retrieving the service's shared singleton.
 
 Subclasses **MUST** override this method. Using this or any equivalent code:
    + (<#MIService subclass#> *)sharedService
    {
        static <#MIService subclass#> *_instance = nil;
 
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _instance = [[self alloc] init];
            NSParameterAssert(_instance != nil);
        });
    
        return _instance;
    }
 
 @return The singleton instance of this class.
 */
+ (MIService *)sharedService;

///---------------------------------------------------------------------------------------
/// @name Properties
///---------------------------------------------------------------------------------------

/** Determines whether the service is available AND it's accepting import requests, at the moment the value it's requested.
 
 Subclasses **MUST** override this method.
 
 @warning The value returned by this method shouldn't be cached: a service can become unavailable while your application is running and if you cache this value you wouldn't notice the change. Instead, call this method every time you need it.
 */
@property (readonly) BOOL isAvailable;

/** Displayable, possibly localized, name of the service.
 
 This can be localized by using standard Foundation functions, but it isn't guaranteed that it is. Subclasses should add the required strings to MediaImport.bundle if they want to provide i18n support.
 
 Subclasses should **NOT** override this method. It's value should be set in the `init` method.
 */
@property (retain, readonly) NSString *displayName;

/** Path to an icon for the service, located inside MediaImport resource bundle.
 
 The image should be retrieved with `[UIImage imageWithContentsOfFile:]` and then used by client apps to identify the service. 
 
 Subclasses should **NOT** override this method. It's value should be set in the `init` method.
 */
@property (retain, readonly) NSString *displayIconPath;
/** Cydia URL to the install page of the service, should be used by client apps to let users install the service.
 
 Subclasses should **NOT** override this method. It's value should be set in the `init` method.
 */
@property (retain, readonly) NSURL *servicePackageURL;

///---------------------------------------------------------------------------------------
/// @name Instance methods
///---------------------------------------------------------------------------------------

/** Triggers the import process with the current service, using the files specified in `pathArray`.
 
 Client applications should use this method when importing more than one file.
 
 Subclasses **MUST** override this method.
 Subclasses should validate `pathArray` and throw an exception if it doesn't meet this requirements: `NS(Mutable)Array`, non-nil and non-zero array,  `NS(Mutable)String`s only, non-nil and non-empty strings representing valid paths.
 
 @param pathArray An array of at least one valid path (represented as `NSString`) to a file supported by the service.
 */
- (void)importFilesInArray:(NSArray *)pathArray;

/** Triggers the import process with the current service, using the file specified by `path`.
 
 Client applications should use this method when importing a single file.
 
 Subclasses **COULD** override this method.
 The default implementation of this method calls importFilesInArray: with an array containg only the string `path`. Subclasses can provide better implementation.
 If you override this method, you should throw an exception if requirements for the `path` argument aren't met.
 
 @param path A non-nil and non-empty string that **MUST** represent a valid path to a file supported by the service.
 */
- (void)importFileAtPath:(NSString *)path;

/** Dictionary containing the supported features of the service.
 
 Example features you can check are: supported file types and metadata tags. __link to docs__
 
  Subclasses **MUST** override this method.
 */

- (id)valueForFeature:(MIServiceFeatures)feature;

@end
