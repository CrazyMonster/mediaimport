//
//  MIGremlinService.m
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MediaImport.h>

#import <MediaImport/MIOpenInService.h>
#import "MIService+Private.h"

#import <UIKit/UIKit.h>

@implementation MIOpenInService

+ (MIOpenInService *)sharedService
{
    static MIOpenInService *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        NSParameterAssert(_instance != nil);
    });
    
    return _instance;
}

- (id)init
{
    if (self = [super init]) {
        self.displayName = @"Open In...";
        self.displayIconPath = MIImageInResourcesBundle(@"icon_openin", @"png");
        self.servicePackageURL = nil;
    }
    return self;
}

- (id)valueForFeature:(MIServiceFeatures)feature
{
    if ([feature isEqual:MIImportLeavesApplication]) return @YES;
    
    if ([feature isEqual:MISupportsSongTitleMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongArtistMetadata])         return @YES;
    if ([feature isEqual:MISupportsSongAlbumMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongAlbumArtworkMetadata])   return @YES;
    
    
    if ([feature isEqual:MISupportsAudioFiles])     return @YES;
    if ([feature isEqual:MISupportsContactFiles])   return @YES;
    if ([feature isEqual:MISupportsImageFiles])     return @YES;
    if ([feature isEqual:MISupportsTextFiles])      return @YES;
    if ([feature isEqual:MISupportsVideoFiles])     return @YES;
    
    return @NO;
}

- (BOOL)isAvailable
{
    return YES;
}

- (void)importFilesInArray:(NSArray *)pathArray
{
    if (pathArray.count == 1)
        [self importFileAtPath:pathArray[0]];
}

- (void)importFileAtPath:(NSString *)path
{
    NSURL *fileURL = [NSURL fileURLWithPath:path];
    UIDocumentInteractionController *controller = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    controller.delegate = self;
    
    UIWindow *window = [[UIApplication sharedApplication] delegate].window;
    [controller presentOpenInMenuFromRect:window.frame inView:window animated:YES];
}

@end
