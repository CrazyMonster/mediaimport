//
//  MIGremlinService.h
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIService.h>

#import <UIKit/UIKit.h>

/** The class that provides support for import with the Apple provided Open In... feature.
 
 Properties of this service:
 
 * `displayName`: _Open In..._
 * `displayIconPath`: path to file _icon_openin.png_
 * `servicePackageURL`: `nil`
 * `importWillCloseApplication`: _YES_
 
 This supports every file that can be opened by another app installed on the user device.
 
 */

@interface MIOpenInService : MIService <UIDocumentInteractionControllerDelegate>

@end
