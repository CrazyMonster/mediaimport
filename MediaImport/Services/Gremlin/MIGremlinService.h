//
//  MIGremlinService.h
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIService.h>

/** The class that provides support for import with Gremlin by [Cocoanuts](http://www.cocoanuts.co/). This is a free tweak that only provides media import. As the last version of Gremlin is still in beta, support is provided for every version of this library and `servicePackageURL` is a search URL.
 
 Properties of this service:
 
 * `displayName`: _Gremlin_
 * `displayIconPath`: path to file _icon_gremlin.png_
 * `servicePackageURL`: search _Gremlin_
 * `importWillCloseApplication`: _NO_
 
 Supported file types of this service are:
 
 *  STILL
 *  TO
 *  DO
 
 */

@interface MIGremlinService : MIService

@end
