//
//  MIGremlinService.m
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MediaImport.h>

#import <MediaImport/MIGremlinService.h>
#import "MIService+Private.h"

#import <objc/runtime.h>
#import <dlfcn.h>
#import <Gremlin/Gremlin.h>

@interface MIGremlinService ()

@property (nonatomic) void *GremlinFramework;
@property (nonatomic) Class GremlinClass;

@end

@implementation MIGremlinService

+ (MIGremlinService *)sharedService
{
    static MIGremlinService *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        NSParameterAssert(_instance != nil);
    });
    
    return _instance;
}

- (id)init
{
    if (self = [super init]) {
        self.displayName = @"Gremlin";
        self.displayIconPath = MIImageInResourcesBundle(@"icon_gremlin", @"png");
        self.servicePackageURL = MICydiaURL(@"package", @"co.cocoanuts.gremlin");
        
        self.GremlinFramework = dlopen("Gremlin.framework/Gremlin", RTLD_LOCAL);
        if (self.GremlinFramework != NULL) {
            self.GremlinClass = objc_getClass("Gremlin");
        }
    }
    return self;
}

- (void)dealloc
{
    if (self.GremlinFramework != NULL) {
        dlclose(self.GremlinFramework);
    }
}

- (id)valueForFeature:(MIServiceFeatures)feature
{
    if ([feature isEqual:MIImportLeavesApplication]) return @NO;
    
    if ([feature isEqual:MISupportsSongTitleMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongArtistMetadata])         return @YES;
    if ([feature isEqual:MISupportsSongAlbumMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongAlbumArtworkMetadata])   return @YES;
    
    
    if ([feature isEqual:MISupportsAudioFiles])     return @YES;
    if ([feature isEqual:MISupportsContactFiles])   return @YES;
    if ([feature isEqual:MISupportsImageFiles])     return @YES;
    if ([feature isEqual:MISupportsTextFiles])      return @NO;
    if ([feature isEqual:MISupportsVideoFiles])     return @YES;
    
    return @NO;
}

- (BOOL)isAvailable
{
    return [self.GremlinClass haveGremlin];
}

- (void)importFilesInArray:(NSArray *)pathArray
{
    NSMutableArray *gremlinArray = [NSMutableArray arrayWithCapacity:pathArray.count];
    [pathArray enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        NSMutableDictionary *infoDictionary = [NSMutableDictionary dictionaryWithObject:obj forKey:@"path"];
        [gremlinArray addObject:infoDictionary];
    }];
    
    [self.GremlinClass importFiles:gremlinArray];
}

- (void)importFileAtPath:(NSString *)path
{
    [self.GremlinClass importFileAtPath:path];
}

@end
