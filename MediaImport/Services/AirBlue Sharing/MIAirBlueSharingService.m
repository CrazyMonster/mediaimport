//
//  MIAirBlueSharingService.m
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MediaImport.h>

#import <MediaImport/MIAirBlueSharingService.h>
#import "MIService+Private.h"

#import "liblockdown.h"
#import <CommonCrypto/CommonHMAC.h>

#define CENTER_NAME "com.if0rce.import.center"

#define CCHmac_KEY (const UInt8 *)"\x65\xC9\x87\xB7\x62\xA0\x7B\xD3\x65\xAD\xEB\xC4\x1B\xCD\x7B\xAB\x2E\xA1\xDB\x25\x69\x27\xCE\xF5\x4D\xE2\x98\x6D\x04\x46\xAD\xC1"

//KEY_LENGTH and SIGNATURE_LENGTH are indeed equal, but are separated for clarity
#define KEY_LENGTH 0x20     //Not counting the \x00 at End Of String because we are ignoring it
#define SIGNATURE_LENGTH 0x20

@implementation MIAirBlueSharingService

+ (MIAirBlueSharingService *)sharedService
{
    static MIAirBlueSharingService *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        NSParameterAssert(_instance != nil);
    });
    
    return _instance;
}

- (id)init
{
    if (self = [super init]) {
        self.displayName = @"AirBlue Sharing";
        self.displayIconPath = MIImageInResourcesBundle(@"icon_airblue", @"png");
        self.servicePackageURL = MICydiaURL(@"package", @"com.ericday.blued-obex");
    }
    
    return self;
}

- (id)valueForFeature:(MIServiceFeatures)feature
{
    if ([feature isEqual:MIImportLeavesApplication]) return @NO;
    
    if ([feature isEqual:MISupportsSongTitleMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongArtistMetadata])         return @YES;
    if ([feature isEqual:MISupportsSongAlbumMetadata])          return @NO;
    if ([feature isEqual:MISupportsSongAlbumArtworkMetadata])   return @YES;

    
    if ([feature isEqual:MISupportsAudioFiles])     return @YES;
    if ([feature isEqual:MISupportsContactFiles])   return @YES;
    if ([feature isEqual:MISupportsImageFiles])     return @YES;
    if ([feature isEqual:MISupportsTextFiles])      return @YES;
    if ([feature isEqual:MISupportsVideoFiles])     return @YES;
    
    return @NO;
}

- (BOOL)isAvailable
{
    CFMessagePortRef messagePort = CFMessagePortCreateRemote(kCFAllocatorDefault, CFSTR(CENTER_NAME));
    BOOL result;
    
    if (messagePort == NULL) {
        result = NO;
    } else {
        result = CFMessagePortIsValid(messagePort);
        CFRelease(messagePort);
    }
    return result;
}

- (void)importFilesInArray:(NSArray *)pathArray
{
    //  1. Create a dictionary with the data to pass to AirBlue deamon
    NSDictionary *importDictionary = @{
    @"center": [MediaImport sharedInstance].applicationIdentifier,
    @"remove": @NO,
    @"import": pathArray
    };
    
    //  2. Get XML representation
    CFDataRef plistXML = CFPropertyListCreateXMLData(kCFAllocatorDefault, (__bridge CFPropertyListRef)(importDictionary));
    NSParameterAssert(plistXML != NULL);
    
    //  3. Get a pointer to the XML data
    const UInt8 *plistBytes = CFDataGetBytePtr(plistXML);
    CFIndex plistLength = CFDataGetLength(plistXML);
    NSParameterAssert(plistBytes != NULL);
    
    //  4. Alloc a buffer for data + signature
    UInt8 *buffer = malloc(plistLength + SIGNATURE_LENGTH);
    //  5. Alloc a buffer for key
    UInt8 *mutableKey = malloc(KEY_LENGTH);
    
    NSParameterAssert(buffer != NULL);
    NSParameterAssert(mutableKey != NULL);
    
    //  6. Fill the buffers
    memcpy(buffer, plistBytes, plistLength);
    memcpy(mutableKey, CCHmac_KEY, KEY_LENGTH);
    
    //  7. Request the Bluetooth MAC address to liblockdown
    LockdownConnectionRef l = lockdown_connect();
    CFStringRef bluetoothMAC = lockdown_copy_value(l, NULL, kLockdownBluetoothAddressKey);
    lockdown_disconnect(l);
    
    NSParameterAssert(bluetoothMAC != NULL);
    
    //8. Get Bluetooth MAC address C string in either system encoding or UTF8
    const char *bluetoothMAC_cstring = CFStringGetCStringPtr(bluetoothMAC, CFStringGetSystemEncoding());
    if (bluetoothMAC_cstring == NULL) {
        bluetoothMAC_cstring = CFStringGetCStringPtr(bluetoothMAC, kCFStringEncodingUTF8);
    }
    
    NSParameterAssert(bluetoothMAC_cstring != NULL);
    
    //  9. Set up a pointer to the first character in bluetoothMAC_cstring
    const char *bluetoothMAC_ptr = &bluetoothMAC_cstring[0];
    
    //  10. Perform a XOR operation between mutableKey and bluetoothMAC_cstring,
    //      storing the result directly in mutableKey.
    //      Since a Bluetooth MAC is 0x12 bytes long and the key is 0x20 bytes long,
    //      when End Of String is detected, bluetoothMAC_ptr is reset to to the first
    //      character in bluetoothMAC_cstring.
    for (NSUInteger index = 0; index < KEY_LENGTH; index++) {
        mutableKey[index] = mutableKey[index] ^ *bluetoothMAC_ptr;
        
        bluetoothMAC_ptr++;
        if (*bluetoothMAC_ptr == '\x00') {
            bluetoothMAC_ptr = &bluetoothMAC_cstring[0];
        }
    }
    
    //  11. Get a pointer to the last part of the buffer, the one holding the signature
    UInt8 *signature = &buffer[plistLength];
    CCHmac(kCCHmacAlgSHA256, mutableKey, KEY_LENGTH, plistBytes, plistLength, signature);
    
    //  12. Perform a XOR operation between the signature and the original key,
    //      storing the result directly in the signature.
    for (NSUInteger index = 0; index < SIGNATURE_LENGTH; index++) {
        signature[index] = mutableKey[index] ^ signature[index];
    }
    
    //  13. Create a CFData structure that holds the final data
    CFDataRef finalData = CFDataCreate(kCFAllocatorDefault, buffer, plistLength + SIGNATURE_LENGTH);
    
    //  14. Create a the remote CFMessagePort and check it's validity
    CFMessagePortRef messagePort = CFMessagePortCreateRemote(kCFAllocatorDefault, CFSTR(CENTER_NAME));
    if (messagePort != NULL) {
        if (CFMessagePortIsValid(messagePort)) {
            //  15. Send the message to AirBlue deamon
            CFMessagePortSendRequest(messagePort, 0xCEFAEDFE, finalData, 5.0, 0, NULL, NULL);
            
            CFMessagePortInvalidate(messagePort);
        }
        CFRelease(messagePort);
    }
    
    //  16. Do the cleanup: release objects and free memory
    CFRelease(plistXML);
    
    free(buffer);
    free(mutableKey);
    
    CFRelease(bluetoothMAC);
    
    CFRelease(finalData);
}
@end
