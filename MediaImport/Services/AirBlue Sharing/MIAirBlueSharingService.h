//
//  MIAirBlueSharingService.h
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIService.h>

/** The class that provides support for import with [AirBlue Sharing](http://www.if0rce.com/en/airblue-sharing-2/) by Eric Day. This is a paid tweak that also enables Bluetooth file transfers. Support is provided only for the official, legit, version of this tweak: cracked version won't be supported.
 
 Properties of this service:
 
 * `displayName`: _AirBlue Sharing_
 * `displayIconPath`: path to file _icon_airblue.png_
 * `servicePackageURL`: package _com.ericday.blued-obex_
 * `importWillCloseApplication`: _NO_

 Supported file types of this service are:
 
 *  STILL
 *  TO
 *  DO
 
 */

@interface MIAirBlueSharingService : MIService

@end
