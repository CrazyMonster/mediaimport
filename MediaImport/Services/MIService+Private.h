//
//  MIService_Private.h
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIService.h>

#define MIResourcesBundle [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"MediaImport" ofType:@"bundle"]]
#define MIImageInResourcesBundle(imageName, type) [MIResourcesBundle pathForResource:imageName ofType:type]

#define MICydiaURL(type, name) [NSURL URLWithString:@"cydia://"type"/"name]

@interface MIService ()

@property (retain, readwrite) NSString *displayName;
@property (retain, readwrite) NSString *displayIconPath;
@property (retain, readwrite) NSURL *servicePackageURL;

@property (retain, readwrite) NSDictionary *serviceCapabilities;

@end
