//
//  MIService.m
//  MediaImport
//
//  Created by CrazyMonster on 18/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIService.h>
#import "MIService+Private.h"

@implementation MIService

+ (MIService *)sharedService
{
   NSAssert([self isMemberOfClass:[MIService class]] == NO, @"MIService is abstract and shouldn't be used directly");
    return nil;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", self.displayName];
}
- (BOOL)isAvailable
{
    return NO;
}
- (void)importFilesInArray:(NSArray *)pathArray
{
    
}
- (void)importFileAtPath:(NSString *)path
{
    [self importFilesInArray:@[ path ]];
}

- (id)valueForFeature:(MIServiceFeatures)feature
{
    return nil;
}

@end
