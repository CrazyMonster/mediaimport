//
//  MIMewSeekService.h
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MIService.h>

/** The class that provides support for import with [MewSeek](http://www.mewseek.net/) by Eric Castro. This is a paid tweak that also enables users to download music and directly import it. Differently from AirBlue Sharing and Gremlin, MewSeek is capable of importing only music and video files.
 
 Properties of this service:
 
 * `displayName`: _MewSeek_
 * `displayIconPath`: path to file _icon_mewseek.png_
 * `servicePackageURL`: search _MewSeek Import Enabler_ (this is a little tweak that enables MediaImport to communicate with MewSeek)
 * `importWillCloseApplication`: _YES_
 
 Supported file types of this service are:
 
 *  STILL
 *  TO
 *  DO
 
 */

@interface MIMewSeekService : MIService

/** Method for retrieving the service's shared singleton.
 
 @return The singleton instance of this class.
 */
+ (MIMewSeekService *)sharedService;

/** The path to the plist file used to communicate with MewSeek.
 
 The plist file will be used only if you try to import multiple files together and will automatically deleted after the import, so it's safe to use the same file multiple times.
 
 The default path is a file in a directory accessible from every app, even sandboxed ones, but it can be changed by simply setting the value of this property.
 */
@property (retain) NSString *pathToImportPlist;

@end
