//
//  MIMewSeekService.m
//  MediaImport
//
//  Created by CrazyMonster on 19/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

#import <MediaImport/MediaImport.h>

#import <MediaImport/MIMewSeekService.h>
#import "MIService+Private.h"

#import <UIKit/UIKit.h>

#define URL_SCHEME [NSURL URLWithString:@"mewseek-import://"]

@implementation MIMewSeekService

+ (MIMewSeekService *)sharedService
{
    static MIMewSeekService *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        NSParameterAssert(_instance != nil);
    });
    
    return _instance;
}

- (id)init
{
    if (self = [super init]) {
        self.displayName = @"MewSeek";
        self.displayIconPath = MIImageInResourcesBundle(@"icon_mewseek", @"png");
        self.servicePackageURL = MICydiaURL(@"package", @"mewseekpro3");

        self.pathToImportPlist = [NSTemporaryDirectory() stringByAppendingPathComponent:@"lastMewseekImport.plist"];
    }
    return self;
}

- (id)valueForFeature:(MIServiceFeatures)feature
{
    if ([feature isEqual:MIImportLeavesApplication]) return @YES;
    
    if ([feature isEqual:MISupportsSongTitleMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongArtistMetadata])         return @YES;
    if ([feature isEqual:MISupportsSongAlbumMetadata])          return @YES;
    if ([feature isEqual:MISupportsSongAlbumArtworkMetadata])   return @YES;
    
    
    if ([feature isEqual:MISupportsAudioFiles])     return @YES;
    if ([feature isEqual:MISupportsContactFiles])   return @NO;
    if ([feature isEqual:MISupportsImageFiles])     return @NO;
    if ([feature isEqual:MISupportsTextFiles])      return @NO;
    if ([feature isEqual:MISupportsVideoFiles])     return @NO;
    
    return @NO;
}

- (BOOL)isAvailable
{
    return [[UIApplication sharedApplication] canOpenURL:URL_SCHEME];
}

- (void)importFilesInArray:(NSArray *)pathArray
{
    NSParameterAssert(pathArray != nil && pathArray.count != 0);
    
    if (pathArray.count == 1) {
        [self importFileAtPath:pathArray[0]];
    } else {
        BOOL plistWriteSuccess = [pathArray writeToFile:self.pathToImportPlist atomically:YES];
        NSParameterAssert(plistWriteSuccess == YES);
        [self importFileAtPath:self.pathToImportPlist];
    }
}

- (void)importFileAtPath:(NSString *)path
{
    NSURL *url = [NSURL URLWithString:path relativeToURL:URL_SCHEME];
    [[UIApplication sharedApplication] openURL:url];
}

@end
