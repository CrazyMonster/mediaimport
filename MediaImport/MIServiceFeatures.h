//
//  MIServiceFeatures.h
//  MediaImport
//
//  Created by CrazyMonster on 31/08/12.
//  Copyright (c) 2012 CrazyMonster. All rights reserved.
//

typedef NSString * MIServiceFeatures;

extern const MIServiceFeatures MIImportLeavesApplication;

extern const MIServiceFeatures MISupportsAudioFiles;
extern const MIServiceFeatures MISupportsContactFiles;
extern const MIServiceFeatures MISupportsImageFiles;
extern const MIServiceFeatures MISupportsTextFiles;
extern const MIServiceFeatures MISupportsVideoFiles;

extern const MIServiceFeatures MISupportsSongTitleMetadata;
extern const MIServiceFeatures MISupportsSongArtistMetadata;
extern const MIServiceFeatures MISupportsSongAlbumMetadata;
extern const MIServiceFeatures MISupportsSongAlbumArtworkMetadata;